using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.Game.Components
{
    [NodeGraphGroupName("Examples")]
    [DisallowMultipleComponent]
    public class Circle : CoreComponent
    {
        public float speed;
        public float radius;

        public float TimeCounter { get; set; } = 0f;
        public Vector3 StartPosition;
        
        private void Awake()
        {
            StartPosition = transform.position;
        }
    }
}