using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.Game.Components
{
    [NodeGraphGroupName("Examples")]
    [DisallowMultipleComponent]
    public class CircleState : CoreComponent
    {
        public bool isMoving = false;
    }
}