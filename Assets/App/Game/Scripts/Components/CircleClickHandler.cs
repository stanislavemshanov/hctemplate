using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;
using UnityEngine.EventSystems;

namespace App.Game.Components
{
    [NodeGraphGroupName("Examples")]
    [DisallowMultipleComponent]
    public class CircleClickHandler : CoreComponent, IPointerDownHandler
    {
        public OutputSignal OnClick;

        public void OnPointerDown(PointerEventData eventData)
        {
            OnClick.Invoke();
        }
    }
}