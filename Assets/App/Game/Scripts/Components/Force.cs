using Krem.AppCore;
using Krem.AppCore.Attributes;
using UnityEngine;

namespace App.Game.Components
{
    [NodeGraphGroupName("Examples")]
    public class Force : CoreComponent
    {
        [SerializeField] private Vector3 _force;

        public Vector3 ForceValue
        {
            get => _force;
            set
            {
                if (value.y < 0)
                {
                    return;
                }

                _force = value;
            }
        }
    }
}