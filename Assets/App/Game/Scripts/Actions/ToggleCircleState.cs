using Krem.AppCore;
using Krem.AppCore.Attributes;

namespace App.Game.Components
{
    [NodeGraphGroupName("Examples")]
    public class ToggleCircleState : CoreAction
    {
        [InjectComponent] private CircleState _circleState;
        protected override bool Action()
        {
            _circleState.isMoving = !_circleState.isMoving;

            return true;
        }
    }
}