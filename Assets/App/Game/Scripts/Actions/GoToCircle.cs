using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.Game.Components
{
    [NodeGraphGroupName("Examples")]
    public class GoToCircle : CoreAction
    {
        public InputComponent<Circle> Circle;

        [InjectComponent] private Transform _transform;
        [InjectComponent] private CircleState _circleState;

        private Vector3 _position = Vector3.zero;

        protected override bool Action()
        {
            if (_circleState.isMoving)
            {
                Circle.Component.TimeCounter += Time.deltaTime * Circle.Component.speed;

                _position = Circle.Component.StartPosition;
                _position.x += Mathf.Cos(Circle.Component.TimeCounter) * Circle.Component.radius;
                _position.z += Mathf.Sin(Circle.Component.TimeCounter) * Circle.Component.radius;

                _transform.position = _position;
            }
            return true;
        }
    }
}