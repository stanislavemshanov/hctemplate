using Krem.AppCore;
using Krem.AppCore.Attributes;
using Krem.AppCore.Ports;
using UnityEngine;

namespace App.Game.Components
{
    [NodeGraphGroupName("Examples")]
    public class AddForce : CoreAction
    {
        public InputComponent<Force> force;
        [InjectComponent] public Rigidbody _rigidbody; 
        protected override bool Action()
        {
            _rigidbody.AddForce(force.Component.ForceValue);


            return true;
        }
    }
}