using UnityEngine;

namespace App.Game.Components.Actions
{
    public class JumpAction : MonoBehaviour
    {
        [Header("Dependencies")]
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private JumpComponent _jumpComponent;
        
        public void Execute()
        {
            _rigidbody.AddForce(_jumpComponent.JumpForce);
        }
    }
}

