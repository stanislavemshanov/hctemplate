using UnityEngine;

namespace App.Game.Components
{
    public class JumpComponent : MonoBehaviour
    {
        [SerializeField] private Vector3 _jumpForce;

        public Vector3 JumpForce 
        {
            get => _jumpForce;
            set
            {
                if (value.y < 0)
                {
                    return;
                }

                _jumpForce = value;
            }
        }
    }
}

