using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace App.Game.Components
{
    public class PointerHandler : MonoBehaviour, IPointerDownHandler
    {
        public UnityEvent OnClick;
        public void OnPointerDown(PointerEventData eventData)
        {
            OnClick.Invoke();
        }
    }
}