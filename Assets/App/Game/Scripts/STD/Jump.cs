using UnityEngine;
using UnityEngine.EventSystems;

namespace App.Game
{
    public class Jump : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private Vector3 _jumpForce;
        [SerializeField] private Rigidbody _rigidbody;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }
        public void DoJump()
        {
            _rigidbody.AddForce(_jumpForce);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            DoJump();
        }
    }
}

